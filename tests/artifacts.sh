#!/bin/bash
set -euo pipefail

function _prepare_s3 {
    local CI_JOB_NAME="$1"
    local CI_JOB_ID="$2"
    local RC_CONTENTS="$3"
    local JOB_PREFIX="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    shift 3

    aws_s3_rm BUCKET_ARTIFACTS "${JOB_PREFIX}"
    if [ -n "$RC_CONTENTS" ]; then
        aws_s3_upload BUCKET_ARTIFACTS - "${JOB_PREFIX}/rc" <<< "${RC_CONTENTS}"
    fi
    for ARTIFACT in "$@"; do
        aws_s3_upload BUCKET_ARTIFACTS - "${JOB_PREFIX}/artifacts/${ARTIFACT}" <<< "${ARTIFACT}"
    done
}

function _clear_s3 {
    aws_s3_rm BUCKET_ARTIFACTS "${CI_PIPELINE_ID}" --recursive
}

function _check_s3_file {
    aws_s3_ls BUCKET_ARTIFACTS "$1" > /dev/null &
    wait $! && s='exists' || s='missing'
    OBSERVED_CONTENTS=$(aws_s3_download BUCKET_ARTIFACTS "$1" - 2> /dev/null || true)
    if [ -n "$2" ]; then
        _check_equal "$s" "exists" "$1 found" "$3"
        _check_equal "${OBSERVED_CONTENTS}" "$2" "$1 contents" "$3"
    else
        _check_equal "$s" "missing" "$1 found" "$3"
    fi
}

function _check_s3 {
    local CI_JOB_NAME="$1"
    local CI_JOB_ID="$2"
    local RC_CONTENTS="$3"
    local JOB_PREFIX="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    shift 3

    _check_s3_file "${JOB_PREFIX}/rc" "${RC_CONTENTS}" "Is the rc file uploaded correctly"

    for ARTIFACT in "$@"; do
        local CONTENTS="${ARTIFACT}"
        if [[ "${ARTIFACT}" = -* ]]; then
            CONTENTS=""
        fi
        _check_s3_file "${JOB_PREFIX}/artifacts/${ARTIFACT#-}" "${CONTENTS}" "Is '${ARTIFACT#-}' uploaded correctly"
    done
}

function _clear_local {
    _prepare_local ''
}

function _prepare_local {
    local RC_CONTENTS="$1"
    shift 1

    rm -rf "${ARTIFACTS_DIR}" "${RC_FILE}" "${ARTIFACTS_DIR}-temp" "${RC_FILE}-temp"
    mkdir -p "${ARTIFACTS_DIR}"
    if [ -n "$RC_CONTENTS" ]; then
        echo "${RC_CONTENTS}" > "${RC_FILE}"
    fi
    for ARTIFACT in "$@"; do
        local ARTIFACT_PATH="${ARTIFACTS_DIR}/${ARTIFACT}"
        mkdir -p "${ARTIFACT_PATH%/*}"
        echo "${ARTIFACT}" > "${ARTIFACT_PATH}"
    done
}

function _check_local_file {
    [ -e "$1" ] && s='exists' || s='missing'
    OBSERVED_CONTENTS=$(cat "$1" 2> /dev/null || true)
    if [ -n "$2" ]; then
        _check_equal "$s" "exists" "$1 found" "$3"
        _check_equal "${OBSERVED_CONTENTS}" "$2" "$1 contents" "$3"
    else
        _check_equal "$s" "missing" "$1 found" "$3"
    fi
}

function _check_local {
    local RC_CONTENTS="$1"
    shift 1

    _check_local_file "${RC_FILE}" "${RC_CONTENTS}" "Is the local rc file in the correct state"
    for ARTIFACT in "$@"; do
        local CONTENTS="${ARTIFACT}"
        if [[ "${ARTIFACT}" = -* ]]; then
            CONTENTS=""
        fi
        _check_local_file "${ARTIFACTS_DIR}/${ARTIFACT#-}" "${CONTENTS}" "Is '${ARTIFACT#-}' in the correct state"
    done

    _check_local_file "${RC_FILE}-temp" '' "Is the temporary rc file removed"
    _check_local_file "${ARTIFACTS_DIR}-temp" '' "Is the temporary artifact directory removed"
}

function check_upload_no_artifacts {
    echo
    echo_yellow "Checking artifact upload without any artifacts for ${artifacts_mode}."
    _clear_local
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local ''
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local ''
            _check_s3 job1 1 ''
            ;;
        s3)
            _check_local ''
            _check_s3 job1 1 ''
            ;;
    esac
}

function check_upload_only_rc {
    echo
    echo_yellow "Checking artifact upload without only rc file for ${artifacts_mode}."
    _prepare_local 'dummy'
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dummy'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy'
            ;;
        s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy'
            ;;
    esac
}

function check_upload_artifacts {
    echo
    echo_yellow "Checking artifact upload with artifacts for ${artifacts_mode}."
    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dummy' 'test.txt'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dummy' 'test.txt'
            _check_s3 job1 1 'dummy' 'test.txt'
            ;;
        s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'test.txt'
            ;;
    esac
}

function check_upload_repeated {
    echo
    echo_yellow "Checking repeated artifact upload with non-size rc changes for ${artifacts_mode}."
    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dummy' 'test.txt'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'test.txt'
            ;;
        s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'test.txt'
            ;;
    esac

    _prepare_local 'dumm2' 'test.txt'
    CI_JOB_NAME=job1 CI_JOB_ID=1 \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dumm2' 'test.txt'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dumm2'
            _check_s3 job1 1 'dumm2' 'test.txt'
            ;;
        s3)
            _check_local 'dumm2'
            _check_s3 job1 1 'dumm2' 'test.txt'
            ;;
    esac
}

function check_upload_artifacts_files {
    echo
    echo_yellow "Checking deletion of artifact files that should (not) be uploaded for ${artifacts_mode}."
    _prepare_local 'dummy' 'archived-file.txt' 'wildcard-file.txt' 'skipped-file.txt'
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 ARTIFACTS="artifacts/archived-file.txt artifacts/wildcard-*.txt artifacts/non-existant-file.txt" \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dummy' 'archived-file.txt' 'wildcard-file.txt' '-skipped-file.txt'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'archived-file.txt' 'wildcard-file.txt' '-skipped-file.txt'
            ;;
        s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'archived-file.txt' 'wildcard-file.txt' '-skipped-file.txt'
            ;;
    esac
}

function check_upload_artifacts_directories {
    echo
    echo_yellow "Checking deletion of artifact directories that should (not) be uploaded for ${artifacts_mode}."
    _prepare_local 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' 'skipped-dir/test.txt'
    _clear_s3
    CI_JOB_NAME=job1 CI_JOB_ID=1 ARTIFACTS="artifacts/archived-dir artifacts/wildcard-* artifacts/non-existent-dir" \
        aws_s3_upload_artifacts

    case "${artifacts_mode}" in
        gitlab)
            _check_local 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' '-skipped-dir/test.txt'
            _check_s3 job1 1 ''
            ;;
        gitlab-s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' '-skipped-dir/test.txt'
            ;;
        s3)
            _check_local 'dummy'
            _check_s3 job1 1 'dummy' 'archived-dir/test.txt' 'wildcard-dir/test.txt' '-skipped-dir/test.txt'
            ;;
    esac
}

function check_download_no_dependency {
    echo
    echo_yellow "Checking artifact download without dependency for ${artifacts_mode}."
    _clear_local
    _clear_s3
    CI_JOB_NAME=job2 CI_JOB_ID=3 \
        aws_s3_download_artifacts

    _check_local ''
}

function check_download_no_artifacts {
    echo
    echo_yellow "Checking artifact download without any artifacts for ${artifacts_mode}."
    _clear_local
    _clear_s3
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts

    _check_local ''
}

function check_download_artifacts {
    echo
    echo_yellow "Checking artifact download with artifacts for ${artifacts_mode}."

    case "${artifacts_mode}" in
        gitlab|gitlab-s3)
            _prepare_local 'dummy' 'test.txt'
            ;;
        s3)
            _prepare_local 'dummy'
            ;;
    esac

    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'
}

function check_download_repeated {
    echo
    echo_yellow "Checking repeated artifact download with artifacts for ${artifacts_mode}."

    case "${artifacts_mode}" in
        gitlab|gitlab-s3)
            _prepare_local 'dummy' 'test.txt'
            ;;
        s3)
            _prepare_local 'dummy'
            ;;
    esac

    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'

    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts

    _check_local 'dummy' 'test.txt'
}

function check_download_newest {
    echo
    echo_yellow "Checking artifact download of the newest job for ${artifacts_mode}."

    case "${artifacts_mode}" in
        gitlab|gitlab-s3)
            _prepare_local 'dummy' 'test2.txt'
            ;;
        s3)
            _prepare_local 'dummy'
            ;;
    esac

    _clear_s3
    _prepare_s3 job1 10 'dummy' 'test2.txt'
    _prepare_s3 job1 9 'dummy' 'test1.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts

    _check_local 'dummy' 'test2.txt'
}

function check_gitlab_s3_rc_gitlab_missing {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that an rc file missing in GitLab is detected in ${artifacts_mode}."

    _prepare_local '' 'test.txt'
    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "rc file difference" "Is the rc file missing in GitLab detected correctly"
}

function check_gitlab_s3_rc_s3_missing {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that an rc file missing in S3 is detected in ${artifacts_mode}."

    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    _prepare_s3 job1 1 '' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "rc file difference" "Is the rc file missing in S3 detected correctly"
}

function check_gitlab_s3_rc_difference {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that differences in the rc file are detected in ${artifacts_mode}."

    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    _prepare_s3 job1 1 'dumm2' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "rc file difference" "Is the rc file difference detected correctly"
}

function check_gitlab_s3_artifact_gitlab_missing {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that an artifact missing in GitLab is detected in ${artifacts_mode}."

    _prepare_local 'dummy'
    _clear_s3
    _prepare_s3 job1 1 'dummy' 'test.txt'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "artifact difference" "Is the artifact missing in GitLab detected correctly"
}

function check_gitlab_s3_artifact_s3_missing {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that an artifact missing in S3 is detected in ${artifacts_mode}."

    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    _prepare_s3 job1 1 'dummy'
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "artifact difference" "Is the artifact missing in S3 detected correctly"
}
function check_gitlab_s3_artifact_difference {
    local artifacts_mode="gitlab-s3"

    echo
    echo_yellow "Checking that differences in artifact size are detected in ${artifacts_mode}."

    _prepare_local 'dummy' 'test.txt'
    _clear_s3
    _prepare_s3 job1 1 'dummy'
    aws_s3_upload BUCKET_ARTIFACTS - "${CI_PIPELINE_ID}/job1/1/artifacts/test.txt" <<< "test.tyt"
    CI_JOB_NAME=job2 CI_JOB_ID=3 ARTIFACT_DEPENDENCY=job1 \
        aws_s3_download_artifacts &
    wait $! && s='not detected' || s='detected'

    _check_equal "$s" "detected" "rc file difference" "Is the artifact size difference detected correctly"
}

# Allow running the tests locally
if [ ! -v CI_PROJECT_DIR ]; then
    export CI_PROJECT_DIR=/tmp
    export CI_PIPELINE_ID=12345
fi

export ARTIFACTS_DIR="${CI_PROJECT_DIR}/artifacts"
export ARTIFACTS_TEMP_SUFFIX="-temp"
export RC_FILE="${CI_PROJECT_DIR}/rc"
mkdir -p "${ARTIFACTS_DIR}"

eval "$(tests/setup-minio.sh)"

source scripts/functions.sh
source tests/helpers.sh

say "artifacts"

_failed_init

for artifacts_mode in gitlab gitlab-s3 s3; do
    check_upload_no_artifacts
    check_upload_only_rc
    check_upload_artifacts
    check_upload_repeated
    check_upload_artifacts_files
    check_upload_artifacts_directories

    check_download_no_dependency
    check_download_no_artifacts
    check_download_artifacts
    check_download_repeated
    check_download_newest
done

check_gitlab_s3_rc_gitlab_missing
check_gitlab_s3_rc_s3_missing
check_gitlab_s3_rc_difference
check_gitlab_s3_artifact_gitlab_missing
check_gitlab_s3_artifact_s3_missing
check_gitlab_s3_artifact_difference

rm -rf "${ARTIFACTS_DIR}"

_failed_check
